<?php

declare(strict_types=1);

namespace Glavvra4\TelegramBundle\Bots\Exception;

class InvalidUpdateTypeException extends \InvalidArgumentException {}
