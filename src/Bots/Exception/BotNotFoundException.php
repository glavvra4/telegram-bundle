<?php

declare(strict_types=1);

namespace Glavvra4\TelegramBundle\Bots\Exception;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BotNotFoundException extends NotFoundHttpException {}
