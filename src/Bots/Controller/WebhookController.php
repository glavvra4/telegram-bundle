<?php

declare(strict_types=1);

namespace Glavvra4\TelegramBundle\Bots\Controller;

use Glavvra4\TelegramBotApi\Type\Update;
use Glavvra4\TelegramBundle\Bots\BotRegistry;
use Glavvra4\TelegramBundle\Bots\Enum\UpdateRuntimeEnum;
use Glavvra4\TelegramBundle\Bots\Exception\BotNotFoundException;
use Glavvra4\TelegramBundle\Bots\UpdateDispatcher\BotUpdateDispatcher;
use Psr\Log\LoggerAwareTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

class WebhookController extends AbstractController
{
    use LoggerAwareTrait;

    private const string SECRET_TOKEN_HEADER = 'X-Telegram-Bot-Api-Secret-Token';

    public function __construct(
        private readonly BotRegistry $botRegistry,
        private readonly BotUpdateDispatcher $botApiUpdateDispatcher,
        private readonly SerializerInterface $serializer,
    ) {}

    /**
     * @throws BotNotFoundException
     */
    public function __invoke(string $alias, Request $request): Response
    {
        $bot = $this->botRegistry->getBotByAlias($alias);

        if ($request->headers->get(self::SECRET_TOKEN_HEADER) !== $bot->secret) {
            throw new UnauthorizedHttpException(
                challenge: $alias,
                message: 'Incorrect secret token',
            );
        }

        try {
            /** @var Update $update */
            $update = $this->serializer->deserialize(
                data: $request->getContent(),
                type: Update::class,
                format: JsonEncoder::FORMAT
            );

            $this->botApiUpdateDispatcher->onUpdate(
                bot: $bot,
                update: $update,
                runtime: UpdateRuntimeEnum::Webhook,
            );

            $this->logger?->info(\sprintf('Event for Telegram update <info>%d</info> dispatched successfully', $update->update_id));
        } catch (\Throwable $e) {
            $this->logger?->error((string) $e);
        }

        return new Response();
    }
}
