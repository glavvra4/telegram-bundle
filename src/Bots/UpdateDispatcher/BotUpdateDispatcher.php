<?php

declare(strict_types=1);

namespace Glavvra4\TelegramBundle\Bots\UpdateDispatcher;

use Glavvra4\TelegramBotApi\Type\Update;
use Glavvra4\TelegramBundle\Bots\Bot;
use Glavvra4\TelegramBundle\Bots\Enum\UpdateRuntimeEnum;
use Glavvra4\TelegramBundle\Bots\Event\UpdateEvent;
use Glavvra4\TelegramBundle\Bots\Helper\BotUpdateHelper;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class BotUpdateDispatcher
{
    public function __construct(
        private EventDispatcherInterface $eventDispatcher,
        private BotUpdateHelper $updateHelper,
    ) {}

    public function onUpdate(Bot $bot, Update $update, UpdateRuntimeEnum $runtime): void
    {
        $event = new UpdateEvent(
            bot: $bot,
            update: $update,
            runtime: $runtime,
        );

        $updateType = $this->updateHelper->getUpdateType($update);

        $this->eventDispatcher->dispatch($event, 'telegram.update');
        $this->eventDispatcher->dispatch($event, \sprintf('telegram.update.%s', $bot->alias));
        $this->eventDispatcher->dispatch($event, \sprintf('telegram.update.%s.%s', $bot->alias, $updateType->value));
    }
}
