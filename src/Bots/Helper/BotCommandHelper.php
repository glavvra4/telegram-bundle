<?php

declare(strict_types=1);

namespace Glavvra4\TelegramBundle\Bots\Helper;

use Glavvra4\TelegramBotApi\Type\Message;
use Glavvra4\TelegramBundle\Bots\Exception\NotABotCommandException;

class BotCommandHelper
{
    private const string BOT_COMMAND_REGEX_PATTERN = '/^(\/[a-z]+)\s?(.*)?$/';

    public function isBotCommand(string $subject): bool
    {
        preg_match(
            pattern: self::BOT_COMMAND_REGEX_PATTERN,
            subject: $subject,
            matches: $matches
        );

        return \count($matches) > 0;
    }

    public function getBotCommandAlias(string $subject): string
    {
        if (!$this->isBotCommand($subject)) {
            throw new NotABotCommandException();
        }

        preg_match(
            pattern: self::BOT_COMMAND_REGEX_PATTERN,
            subject: $subject,
            matches: $matches
        );

        return $matches[1];
    }

    public function getBotCommandArgument(string $subject): string
    {
        if (!$this->isBotCommand($subject)) {
            throw new NotABotCommandException();
        }

        preg_match(
            pattern: self::BOT_COMMAND_REGEX_PATTERN,
            subject: $subject,
            matches: $matches
        );

        return $matches[2];
    }

    public function isBotCommandMessage(Message $message): bool
    {
        if (null === $message->text) {
            return false;
        }

        return $this->isBotCommand($message->text);
    }

    public function getBotCommandAliasFromMessage(Message $message): string
    {
        if (!$this->isBotCommandMessage($message)) {
            throw new NotABotCommandException();
        }

        return $this->getBotCommandAlias($message->text);
    }

    public function getBotCommandArgumentFromMessage(Message $message): string
    {
        if (!$this->isBotCommandMessage($message)) {
            throw new NotABotCommandException();
        }

        return $this->getBotCommandArgument($message->text);
    }
}
