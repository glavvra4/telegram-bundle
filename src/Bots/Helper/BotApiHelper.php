<?php

declare(strict_types=1);

namespace Glavvra4\TelegramBundle\Bots\Helper;

use Glavvra4\TelegramBotApi\Request\RequestInterface;

class BotApiHelper
{
    public function getMethodName(RequestInterface $request): string
    {
        $reflection = new \ReflectionClass($request);

        return $reflection->getShortName();
    }
}
