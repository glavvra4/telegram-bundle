<?php

declare(strict_types=1);

namespace Glavvra4\TelegramBundle\Bots\Helper;

use Glavvra4\TelegramBotApi\Enum\UpdateTypeEnum;
use Glavvra4\TelegramBotApi\Type\Update;
use Glavvra4\TelegramBundle\Bots\Exception\UnknownUpdateTypeException;

class BotUpdateHelper
{
    public function getUpdateType(Update $update): UpdateTypeEnum
    {
        foreach (UpdateTypeEnum::cases() as $case) {
            if (isset($update->{$case->value})) {
                return $case;
            }
        }

        throw new UnknownUpdateTypeException();
    }
}
