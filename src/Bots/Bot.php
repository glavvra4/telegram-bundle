<?php

declare(strict_types=1);

namespace Glavvra4\TelegramBundle\Bots;

use Glavvra4\TelegramBotApi\BotApiConfig;
use Glavvra4\TelegramBotApi\BotApiInterface;
use Glavvra4\TelegramBotApi\ConfigurableBotApiInterface;
use Glavvra4\TelegramBotApi\Request;
use Glavvra4\TelegramBotApi\Type\Message;
use Glavvra4\TelegramBotApi\Type\MessageId;
use Glavvra4\TelegramBotApi\Type\User;
use Glavvra4\TelegramBotApi\Type\WebhookInfo;
use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;

#[AutoconfigureTag('telegram.bot')]
class Bot implements BotApiInterface
{
    private BotApiInterface $api;

    public function __construct(
        BotApiInterface&ConfigurableBotApiInterface $api,
        public readonly string $alias,
        public readonly string $username,
        public readonly string $token,
        public readonly string $secret,
        public readonly string $apiUrl = BotApiConfig::TELEGRAM_API_URL,
        public readonly int $clientTimeout = BotApiConfig::DEFAULT_CLIENT_TIMEOUT,
        public readonly int $longPollingTimeout = BotApiConfig::DEFAULT_CLIENT_TIMEOUT - 1,
    ) {
        $this->api = $api->withConfig(new BotApiConfig(
            token: $this->token,
            apiUrl: $this->apiUrl,
            clientTimeout: $this->clientTimeout
        ));
    }

    public function getMe(Request\GetMe $request): User
    {
        return $this->api->getMe($request);
    }

    public function sendMessage(Request\SendMessage $request): Message
    {
        return $this->api->sendMessage($request);
    }

    public function copyMessage(Request\CopyMessage $request): MessageId
    {
        return $this->api->copyMessage($request);
    }

    public function approveChatJoinRequest(Request\ApproveChatJoinRequest $request): bool
    {
        return $this->api->approveChatJoinRequest($request);
    }

    public function answerCallbackQuery(Request\AnswerCallbackQuery $request): bool
    {
        return $this->api->answerCallbackQuery($request);
    }

    public function deleteWebhook(Request\DeleteWebhook $request): bool
    {
        return $this->api->deleteWebhook($request);
    }

    public function getWebhookInfo(Request\GetWebhookInfo $request): WebhookInfo
    {
        return $this->api->getWebhookInfo($request);
    }

    public function getUpdates(Request\GetUpdates $request): array
    {
        return $this->api->getUpdates($request);
    }

    public function answerInlineQuery(Request\AnswerInlineQuery $request): bool
    {
        return $this->api->answerInlineQuery($request);
    }

    public function sendInvoice(Request\SendInvoice $request): Message
    {
        return $this->api->sendInvoice($request);
    }

    public function answerPreCheckoutQuery(Request\AnswerPreCheckoutQuery $request): bool
    {
        return $this->api->answerPreCheckoutQuery($request);
    }

    public function setPassportDataErrors(Request\SetPassportDataErrors $request): bool
    {
        return $this->api->setPassportDataErrors($request);
    }

    public function editMessageText(Request\EditMessageText $request): Message
    {
        return $this->api->editMessageText($request);
    }

    public function deleteMessage(Request\DeleteMessage $request): bool
    {
        return $this->api->deleteMessage($request);
    }
}
