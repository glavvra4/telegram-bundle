<?php

declare(strict_types=1);

namespace Glavvra4\TelegramBundle\Bots\Command;

use Glavvra4\TelegramBundle\Bots\BotRegistry;
use Glavvra4\TelegramBundle\Bots\Exception\BotNotFoundException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class GetSecretTokenCommand extends Command
{
    public function __construct(
        private readonly BotRegistry $botRegistry,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setName('telegram:get-secret-token');
        $this->setAliases(['tg:gst']);

        $this->setDescription('Generates a secret token for Telegram Bot webhook');

        $this->addArgument(
            name: 'bot',
            mode: InputArgument::REQUIRED,
            description: 'Telegram Bot alias',
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $io->title('Webhook secret token generation');

            $botAlias = $input->getArgument('bot');

            if (!\is_string($botAlias)) {
                throw new BotNotFoundException();
            }

            $bot = $this->botRegistry->getBotByAlias($botAlias);

            $io->writeln($bot->secret);

            $io->success('Token successfully generated');

            return Command::SUCCESS;
        } catch (\Throwable $e) {
            $io->error($e->getMessage());

            return Command::FAILURE;
        }
    }
}
