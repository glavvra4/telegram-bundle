<?php

declare(strict_types=1);

namespace Glavvra4\TelegramBundle\Bots\Command;

use Glavvra4\TelegramBotApi\Request\GetWebhookInfo;
use Glavvra4\TelegramBundle\Bots\BotRegistry;
use Glavvra4\TelegramBundle\Bots\Exception\BotNotFoundException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class GetWebhookInfoCommand extends Command
{
    public function __construct(
        private readonly BotRegistry $botRegistry,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setName('telegram:get-webhook-info');
        $this->setAliases(['tg:gwi']);

        $this->setDescription('Receives Bot API webhook information');

        $this->addArgument(
            name: 'bot',
            mode: InputArgument::REQUIRED,
            description: 'Telegram Bot alias',
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $io->title('Webhook info receiving');

            $botAlias = $input->getArgument('bot');

            if (!\is_string($botAlias)) {
                throw new BotNotFoundException();
            }

            $botApi = $this->botRegistry->getBotByAlias($botAlias);

            $webhookInfo = $botApi->getWebhookInfo(new GetWebhookInfo());

            $io->definitionList(
                ['URL' => $webhookInfo->url ?? '-'],
                ['Has custom certificate' => null !== $webhookInfo->has_custom_certificate ? $webhookInfo->has_custom_certificate ? 'Да' : 'Нет' : '-'],
                ['Pending update count' => $webhookInfo->pending_update_count ?? '-'],
                ['IP address' => $webhookInfo->ip_address ?? '-'],
                ['Last error date' => null !== $webhookInfo->last_error_date ? date('Y-m-d H:i:s', $webhookInfo->last_error_date) : '-'],
                ['Last error message' => $webhookInfo->last_error_message ?? '-'],
                ['Last synchronization error date' => $webhookInfo->last_synchronization_error_date ?? '-'],
                ['Max connections' => $webhookInfo->max_connections ?? '-'],
                ['Allowed updates' => null !== $webhookInfo->allowed_updates ? implode(', ', $webhookInfo->allowed_updates) : '-'],
            );

            $io->success('Webhook info was successfully received');

            return Command::SUCCESS;
        } catch (\Throwable $e) {
            $io->error($e->getMessage());

            return Command::FAILURE;
        }
    }
}
