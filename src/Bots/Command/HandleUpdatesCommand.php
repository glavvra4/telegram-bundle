<?php

declare(strict_types=1);

namespace Glavvra4\TelegramBundle\Bots\Command;

use Glavvra4\TelegramBotApi\Enum\UpdateTypeEnum;
use Glavvra4\TelegramBotApi\Request\GetUpdates;
use Glavvra4\TelegramBundle\Bots\BotRegistry;
use Glavvra4\TelegramBundle\Bots\Enum\UpdateRuntimeEnum;
use Glavvra4\TelegramBundle\Bots\Exception\BotNotFoundException;
use Glavvra4\TelegramBundle\Bots\UpdateDispatcher\BotUpdateDispatcher;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class HandleUpdatesCommand extends Command
{
    public function __construct(
        private readonly BotRegistry $botRegistry,
        private readonly BotUpdateDispatcher $updateDispatcher,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setName('telegram:handle-updates');
        $this->setAliases(['tg:hu']);

        $this->setDescription(description: 'Receives Bot API updates over long-polling and dispatches update events');

        $this->addArgument(
            name: 'bot',
            mode: InputArgument::REQUIRED,
            description: 'Telegram Bot alias',
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $io->title('Started updates handling');

            $botAlias = $input->getArgument('bot');

            if (!\is_string($botAlias)) {
                throw new BotNotFoundException();
            }

            $bot = $this->botRegistry->getBotByAlias($botAlias);

            $updates = $bot->getUpdates(new GetUpdates(
                timeout: $bot->longPollingTimeout,
                allowed_updates: UpdateTypeEnum::cases()
            ));

            $lastUpdateId = -1;

            $updatesCount = \count($updates);
            $successfullyDispatchedUpdatesCount = 0;

            foreach ($updates as $update) {
                $lastUpdateId = $update->update_id;

                try {
                    $this->updateDispatcher->onUpdate(
                        bot: $bot,
                        update: $update,
                        runtime: UpdateRuntimeEnum::LongPolling,
                    );

                    ++$successfullyDispatchedUpdatesCount;

                    $io->writeln(\sprintf('Event for Telegram update <info>%d</info> dispatched successfully', $update->update_id));
                } catch (\Throwable $e) {
                    $io->writeln(\sprintf('<error>Error occurred when dispatching event for update <info>%d</info>: %s</error>', $update->update_id, $e->getMessage()));
                    $io->writeln(\sprintf('<error>%s</error>', $e->getTraceAsString()));
                }
            }

            $bot->getUpdates(new GetUpdates(
                offset: $lastUpdateId + 1,
                timeout: 0,
                allowed_updates: UpdateTypeEnum::cases(),
            ));

            $io->success("Successfully dispatched events for {$successfullyDispatchedUpdatesCount} of {$updatesCount} updates.");

            return Command::SUCCESS;
        } catch (\Throwable $e) {
            $io->error($e->getMessage());

            return Command::FAILURE;
        }
    }
}
