<?php

declare(strict_types=1);

namespace Glavvra4\TelegramBundle\Bots\Event;

use Glavvra4\TelegramBotApi\Type\Update;
use Glavvra4\TelegramBundle\Bots\Bot;
use Glavvra4\TelegramBundle\Bots\Enum\UpdateRuntimeEnum;
use Symfony\Contracts\EventDispatcher\Event;

class UpdateEvent extends Event
{
    public function __construct(
        public readonly Bot $bot,
        public readonly Update $update,
        public readonly UpdateRuntimeEnum $runtime,
    ) {}
}
