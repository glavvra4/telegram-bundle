<?php

declare(strict_types=1);

namespace Glavvra4\TelegramBundle\Bots\Enum;

enum UpdateRuntimeEnum: string
{
    case Webhook = 'webhook';
    case LongPolling = 'long-polling';
}
