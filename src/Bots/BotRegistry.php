<?php

declare(strict_types=1);

namespace Glavvra4\TelegramBundle\Bots;

use Glavvra4\TelegramBundle\Bots\Exception\BotNotFoundException;
use Symfony\Component\DependencyInjection\Attribute\AutowireLocator;
use Symfony\Contracts\Service\ServiceCollectionInterface;

class BotRegistry
{
    private array $bots = [];

    /**
     * @param ServiceCollectionInterface<Bot> $locator
     */
    public function __construct(
        #[AutowireLocator('telegram.bot')]
        ServiceCollectionInterface $locator,
    ) {
        /** @var Bot $bot */
        foreach ($locator as $bot) {
            $this->bots[$bot->alias] = $bot;
        }
    }

    public function findBotByAlias(string $alias): ?Bot
    {
        return $this->bots[$alias] ?? null;
    }

    /**
     * @throws BotNotFoundException
     */
    public function getBotByAlias(string $alias): Bot
    {
        return $this->findBotByAlias($alias)
            ?? throw new BotNotFoundException("Bot \"{$alias}\" not found. Available are: ".implode(', ', $this->getAliases()));
    }

    /**
     * @return string[]
     */
    public function getAliases(): array
    {
        return array_keys($this->bots);
    }
}
